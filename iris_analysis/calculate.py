import math

def mean(data):
    sume = 0
    count = len(data)
    for i in data:
        sume += float(i)
    return sume/count
def median(data):
    count = len(data)
    return data[int(count/2)]
def std(data):
    m = float(mean(data))
    sume = 0
    count = len(data)
    for i in data:
        sume += (m-float(i))**2
    return math.sqrt(sume/count)

