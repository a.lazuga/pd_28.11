def load_file(file_path):
    plik = open(file_path, "r")
    sepal_length = []
    sepal_width = []
    petal_length = []
    petal_width = []
    for i in plik:
        linia = i.split(",")
        sepal_length.append(linia[0])
        sepal_width.append(linia[1])
        petal_length.append(linia[2])
        petal_width.append(linia[3])
    return sepal_length, sepal_width, petal_length, petal_width

