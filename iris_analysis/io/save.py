import csv

def save_result(calculations, result_file):
    file = open(result_file, 'w')
    writer = csv.writer(file)
    writer.writerow(['Column name', 'Median', 'Mean', 'Std'])
    writer.writerows(calculations)
