from iris_analysis import median, mean, std
from iris_analysis.io import load_file, save_result

def main():
    calculations = []
    print("Add path to the input file")
    input_file = input()
    print("Add path to the result file")
    result_file = input()
    data = load_file(input_file)
    for i in data:
        result = []
        result.append(i[0].strip())
        result.append(median(i[1::]))
        result.append(mean(i[1::]))
        result.append(std(i[1::]))
        calculations.append(result)
    save_result(calculations, result_file)

main()
